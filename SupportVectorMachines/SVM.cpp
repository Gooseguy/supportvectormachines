//
//  SVM.cpp
//  SupportVectorMachines
//
//  Created by Christian on 12/27/14.
//  Copyright (c) 2014 Christian Cosgrove. All rights reserved.
//

#include "SVM.h"
#include "picopng.h"
#include <errno.h>
#include <fstream>
#include "EigenQP/EigenQP.h"
#include <iostream>
#include "glm/gtx/norm.hpp"
static const GLfloat g_quad_vertex_buffer_data[] = {
    -1.0f, -1.0f, 0.0f,
    1.0f, -1.0f, 0.0f,
    -1.0f,  1.0f, 0.0f,
    -1.0f,  1.0f, 0.0f,
    1.0f, -1.0f, 0.0f,
    1.0f,  1.0f, 0.0f,
};


SVM::SVM()
{
    generateBuffers();
}

std::string get_file_contents(const char *filename);


void SVM::loadPNG(const std::string &address)
{
    
    std::vector<unsigned char> out;
    unsigned long width, height;
    
    std::string in=  get_file_contents(address.c_str());
    
    
    decodePNG(out, width, height, (const unsigned char*)&in[0], in.size());
    
    
    images.push_back(Image(width, height, out));
}

float i = 0;
void SVM::Draw(GLManager& glManager)
{
    glManager.Programs[0].SetTexture("currTexture", 0);
    
    glBindVertexArray(vao);
    
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
}

//Efficient file loading, from
//http://insanecoding.blogspot.com/2011/11/how-to-read-in-file-in-c.html
std::string get_file_contents(const char *filename)
{
    std::ifstream in(filename, std::ios::in | std::ios::binary);
    if (in)
    {
        std::string contents;
        in.seekg(0, std::ios::end);
        contents.resize(in.tellg());
        in.seekg(0, std::ios::beg);
        in.read(&contents[0], contents.size());
        in.close();
        return(contents);
    }
    throw(errno);
}

void SVM::generateBuffers()
{
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_quad_vertex_buffer_data), g_quad_vertex_buffer_data, GL_STATIC_DRAW);
    
    glBindVertexArray(0);
}


void SVM::AddImage(const std::string &address, bool isExample)
{
    loadPNG(address);
    examples.push_back(isExample);
}

void SVM::Solve()
{

    
    /*
    min 0.5 * x G x + g0 x
    s.t.
    CE^T x + ce0 = 0
    CI^T x + ci0 >= 0
    
    The matrix and vectors dimensions are as follows:
G: n * n
g0: n
    
CE: n * p
ce0: p
    
CI: n * m
ci0: m
    
x: n
     
     
     g=images.size()
     p=1
     
     */
    
    int n=images.size(), m=1, p=n;
    Eigen::MatrixXd G(n,n), CI(n,p), CE(n,m);
    Eigen::VectorXd g0(n), ce(m), ci(p);
    alpha = Eigen::VectorXd(n);
    for (int i=0; i<images.size();i++)
    {
        for (int j = 0; j<images.size();j++)
        {
            G(i,j) = (examples[i]==examples[j] ? 1.0 : -1.0) * kernel(images[i], images[j]);
        }
    }
//    G = 
//    
    for (int i = 0; i<images.size();i++) g0(i)=-1.0;
    ce(0)=0.0;
    for (int i = 0; i<images.size();i++)
        CE(i) = examples[i] ? 1.0 : -1.0;
    for (int i = 0;i<images.size();i++) for(int j = 0; j<m;j++)
        CI(i,j)=i==j ? 1 : 0;
    Eigen::VectorXd ci0(m);
    for (int i = 0; i<images.size();i++)
        ci(i)=0.0f;
    
    std::cout << "Cost of solution:" << std::endl;
    std::cout << QP::solve_quadprog(G, g0, CE, ce, CI, ci, alpha) << std::endl;
    std::cout << alpha << std::endl;
}

float SVM::kernel(const Image &image1, const Image &image2) const
{
    if (image1.WIDTH!=image2.WIDTH || image1.HEIGHT!=image2.HEIGHT) throw std::out_of_range("Image sizes do not match!");
//    float a =0;
//    for (int i = 0; i<image1.WIDTH * image1.HEIGHT;i++)
//    {
//        a+=glm::dot(image1.GetFloatPixel(i),image2.GetFloatPixel(i));
//    }
//    return a*10e-5f;
    const float lambda = 1.0f;
    float a = 0;
    for (int i = 0; i<image1.WIDTH*image1.HEIGHT;i++)
    {
        a+=(image1.GetFloatPixel(i).r - image2.GetFloatPixel(i).r)*(image1.GetFloatPixel(i).r - image2.GetFloatPixel(i).r);
    }
    return std::exp(-lambda * a);
}
