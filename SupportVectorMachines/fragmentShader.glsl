#version 330 core

uniform sampler2D currTexture;

out vec3 color;

in vec2 fragCoord;

void main()
{
    color = texture(currTexture,fragCoord).rgb;
}