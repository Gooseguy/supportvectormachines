//
//  SVM.h
//  SupportVectorMachines
//
//  Created by Christian on 12/27/14.
//  Copyright (c) 2014 Christian Cosgrove. All rights reserved.
//
#pragma once
#include "string"
#include <vector>
#include "Image.h"
#include "GLManager.h"
#include "Eigen/Eigen"

class SVM
{
public:
    SVM();
    
    
    
    void Draw(GLManager& glManager);
    void Update();
    void AddImage(const std::string& address, bool isExample);
    void Solve();
    
private:
    std::vector<Image> images;
    std::vector<bool> examples;
    
    void loadPNG(const std::string& address);
    
    GLuint vao, vbo;
    
    Eigen::VectorXd alpha;
    Eigen::VectorXd W;
    float bias;
    
    
    void generateBuffers();
    float kernel(const Image& image1, const Image& image2) const;
    
};


