//
//  MainGame.h
//  SupportVectorMachines
//
//  Created by Christian on 12/27/14.
//  Copyright (c) 2014 Christian Cosgrove. All rights reserved.
//
#pragma once
#include "SDL2/SDL.h"
#include "GLManager.h"
#include "SVM.h"

class MainGame
{
public:
    enum class State { RUN, EXIT} State = State::RUN;
    
    MainGame();
private:
    SDL_Window* window;
    void handleEvents();
    void update();
    void draw(GLManager& glManager, SVM& svm);
    void loadImages(const std::string& directory, SVM& svm);
};