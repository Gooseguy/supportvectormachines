//
//  MainGame.cpp
//  SupportVectorMachines
//
//  Created by Christian on 12/27/14.
//  Copyright (c) 2014 Christian Cosgrove. All rights reserved.
//

#include "MainGame.h"
#include<stdexcept>
#include <string>
#include "ResourcePath.hpp"
#include "Eigen/Eigen"
#include <iostream>
#include <fstream>
MainGame::MainGame()
{
    if (SDL_Init(SDL_INIT_VIDEO)) throw std::logic_error("Failed to initialize SDL.  " + std::string(SDL_GetError()));
    
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
    
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetSwapInterval(1);
    
    
    window = SDL_CreateWindow("Net Evolution", 0, 0, 1280,720, SDL_WINDOW_OPENGL);
    if (window==nullptr) throw std::logic_error("Window failed to be initialized");
    SDL_GLContext context = SDL_GL_CreateContext(window);
    if (context==nullptr) throw std::logic_error("SDL_GL could not be initialized!");
    
    GLManager glManager(resourcePath() + "fragmentShader.glsl", resourcePath() + "vertexShader.glsl");
    
    SVM svm;
    loadImages(resourcePath() + "TrainingCats/", svm);
    svm.Solve();
    
    
    while (State!=State::EXIT)
    {
        update();
        draw(glManager, svm);
        handleEvents();
        SDL_GL_SwapWindow(window);
    }
    
}

void MainGame::loadImages(const std::string& directory, SVM& svm)
{
    for (int i = 0;;i++)
    {
        {
            std::ifstream str((directory + "example-" + std::to_string(i) + ".png"), std::ios::in | std::ios::binary);
            if (!str.good()) break;
        }
        svm.AddImage(directory + "example-" + std::to_string(i) + ".png", true);
    }
    for (int i = 0;;i++)
    {
        {
            std::ifstream str((directory + "nonexample-" + std::to_string(i) + ".png"), std::ios::in | std::ios::binary);
            if (!str.good()) break;
        }
        svm.AddImage(directory + "nonexample-" + std::to_string(i) + ".png", false);
    }
}

void MainGame::update()
{
    
}
void MainGame::draw(GLManager& glManager, SVM& svm)
{
    glClear(GL_COLOR_BUFFER_BIT);
    svm.Draw(glManager);
}

void MainGame::handleEvents()
{
    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
            case SDL_QUIT:
                State = State::EXIT;
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.scancode)
            {
                case SDL_SCANCODE_ESCAPE:
                    State = State::EXIT;
                    break;
                default: break;

            }
            default: break;
        }
    }
}