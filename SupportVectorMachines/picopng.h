//
//  picopng.h
//  SupportVectorMachines
//
//  Created by Christian on 12/27/14.
//  Copyright (c) 2014 Christian Cosgrove. All rights reserved.
//
#pragma once
int decodePNG(std::vector<unsigned char>& out_image, unsigned long& image_width, unsigned long& image_height, const unsigned char* in_png, size_t in_size, bool convert_to_rgba32 = true);