#version 330 core

layout(location = 0) in vec3 vertexPosition;


out vec2 fragCoord;
void main()
{
    fragCoord = vertexPosition.xy * vec2(0.5,-0.5) - vec2(0.5,0.5);
    gl_Position = vec4(vertexPosition,1);
}